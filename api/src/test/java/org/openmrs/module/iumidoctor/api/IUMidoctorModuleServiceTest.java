/**
 * The contents of this file are subject to the OpenMRS Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://license.openmrs.org
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * Copyright (C) OpenMRS, LLC.  All Rights Reserved.
 */
package org.openmrs.module.iumidoctor.api;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.openmrs.api.context.Context;
import org.openmrs.module.iumidoctor.IUMidoctorService;
import org.openmrs.notification.MessageService;
import org.openmrs.test.BaseModuleContextSensitiveTest;

/**
 * Tests {@link ${IUMidoctorService}}.
 */
public class  IUMidoctorModuleServiceTest extends BaseModuleContextSensitiveTest {
	private IUMidoctorService service;
	@Before
	public void init(){
		service = Context.getService(IUMidoctorService.class);
	}
	@Test
	public void shouldSetupContext() {		
		assertNotNull(service);
		assertNotNull(Context.getService(MessageService.class));
	}
	@Test
	public void testSendErrorEmail(){
		
	}
	
}
