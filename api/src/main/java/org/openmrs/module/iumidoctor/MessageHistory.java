package org.openmrs.module.iumidoctor;

import java.util.Date;
import org.openmrs.Patient;

public class MessageHistory {
	private String programName;
	private Patient patient;
	private Date date;
	private Integer type;
	private String result;
	
	public final static int TYPE_ALL = 0;
	public final static int TYPE_CALL = 1;
	public final static int TYPE_MESSAGE = 2;
	public final static int TYPE_EMAIL = 3;
	
	public String getProgramName() {
		return programName;
	}
	public void setProgramName(String programName) {
		this.programName = programName;
	}
	public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	
	public String getTypeName(){
		switch(type){
		case TYPE_CALL: return "Call";
		case TYPE_MESSAGE : return "Message";
		case TYPE_EMAIL : return "Email";
		default : return "";
		}
	}
}
