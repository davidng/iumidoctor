package org.openmrs.module.iumidoctor.db;

import java.util.List;
import org.openmrs.module.iumidoctor.CallLog;
import org.openmrs.module.iumidoctor.SmsHistory;
import org.openmrs.module.iumidoctor.AlertOutbox;
import org.openmrs.module.iumidoctor.AlertError;

/**
 * Public Interface to the {@link}HibernateIUMidoctorDAO
 * 
 * @author Samuel Mbugua
 */
public interface IUMidoctorDAO {
	
	public List<CallLog> getAllCallLogs();

	public List<SmsHistory> getAllSmsHistories();
	
	public List<AlertOutbox> getAllAlertOutbox();
	
	public List<AlertError> getAllAlertErrors();
	
	public List<CallLog> getCallLogsByStatus(Integer start, Integer length, Integer status);
        
}