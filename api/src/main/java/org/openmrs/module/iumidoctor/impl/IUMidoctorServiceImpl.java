package org.openmrs.module.iumidoctor.impl;

import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;

import org.openmrs.module.iumidoctor.CallLog;
import org.openmrs.module.iumidoctor.SmsHistory;
import org.openmrs.module.iumidoctor.AlertOutbox;
import org.openmrs.module.iumidoctor.AlertError;
import org.openmrs.module.iumidoctor.MessageHistory;
import org.openmrs.module.iumidoctor.IUMidoctorService;
import org.openmrs.module.iumidoctor.db.IUMidoctorDAO;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Samuel Mbugua
 */
@Transactional
@Component
public class IUMidoctorServiceImpl implements IUMidoctorService {
	        
    private IUMidoctorDAO iUMidoctorDAO;
	
	public IUMidoctorServiceImpl() {
		 
	}

    public IUMidoctorDAO getIUMidoctorDAO() {
        return iUMidoctorDAO;
    }

    public void setIUMidoctorDAO(IUMidoctorDAO iUMidoctorDAO) {
        this.iUMidoctorDAO = iUMidoctorDAO;
    }

	public List<CallLog> getAllCallLogs() {
		return iUMidoctorDAO.getAllCallLogs();
	}

	public List<SmsHistory> getAllSmsHistories(){
		return iUMidoctorDAO.getAllSmsHistories();
	}
	
	public List<AlertOutbox> getAllAlertOutbox(){
		return iUMidoctorDAO.getAllAlertOutbox();
	}
	
	public List<AlertError> getAllAlertErrors(){
		return iUMidoctorDAO.getAllAlertErrors();
	}
	
	public List<CallLog> getCallLogsByStatus(Integer start, Integer length, Integer status) {
		return iUMidoctorDAO.getCallLogsByStatus(start, length, status);
	}
	
	public List<MessageHistory> getMessageHistories(Integer messageType, Integer start, Integer length){
		List<MessageHistory> listMessageHistory = new ArrayList<MessageHistory>();
		if(messageType == MessageHistory.TYPE_CALL) {
			List<CallLog> listCallLog = getAllCallLogs();
			for(CallLog callLog : listCallLog){
				MessageHistory msg = new MessageHistory();
				msg.setProgramName(callLog.getCallTrigger().getName());
				msg.setPatient(callLog.getPatient());
				msg.setDate(callLog.getDateCreated());
				msg.setType(MessageHistory.TYPE_CALL);
				msg.setResult(callLog.getStatusMessage());
				listMessageHistory.add(msg);
			}
		} else if (messageType == MessageHistory.TYPE_MESSAGE) {
			List<SmsHistory> listSmsHistory = getAllSmsHistories();
			for(SmsHistory smsHistory : listSmsHistory){
				MessageHistory msg = new MessageHistory();
				msg.setProgramName(smsHistory.getProgramming().getName());
				msg.setPatient(smsHistory.getPatient());
				msg.setDate(smsHistory.getSentToGatewayDate());
				msg.setType(MessageHistory.TYPE_MESSAGE);
				msg.setResult(smsHistory.getSmsSentResult());
				listMessageHistory.add(msg);
			}
		} else if (messageType == MessageHistory.TYPE_EMAIL) {
			List<AlertOutbox> listAlertOutbox = getAllAlertOutbox();
			for(AlertOutbox alertOutbox : listAlertOutbox){
				MessageHistory msg = new MessageHistory();
				msg.setProgramName(alertOutbox.getAlertName());
				msg.setPatient(alertOutbox.getPatient());
				msg.setDate(alertOutbox.getDateCreated());
				msg.setType(MessageHistory.TYPE_EMAIL);
				msg.setResult("Sent");
				listMessageHistory.add(msg);
			}
			
			List<AlertError> listAlertError = getAllAlertErrors();
			for(AlertError alertError : listAlertError){
				MessageHistory msg = new MessageHistory();
				msg.setProgramName(alertError.getAlertName());
				msg.setPatient(alertError.getPatient());
				msg.setDate(alertError.getDateCreated());
				msg.setType(MessageHistory.TYPE_EMAIL);
				msg.setResult("Not sent");
				listMessageHistory.add(msg);
			}
		} else if (messageType == MessageHistory.TYPE_ALL) {
			//
			List<CallLog> listCallLog = getAllCallLogs();
			for(CallLog callLog : listCallLog){
				MessageHistory msg = new MessageHistory();
				msg.setProgramName(callLog.getCallTrigger().getName());
				msg.setPatient(callLog.getPatient());
				msg.setDate(callLog.getDateCreated());
				msg.setType(MessageHistory.TYPE_CALL);
				msg.setResult(callLog.getStatusMessage());
				listMessageHistory.add(msg);
			}
			//
			List<SmsHistory> listSmsHistory = getAllSmsHistories();
			for(SmsHistory smsHistory : listSmsHistory){
				MessageHistory msg = new MessageHistory();
				msg.setProgramName(smsHistory.getProgramming().getName());
				msg.setPatient(smsHistory.getPatient());
				msg.setDate(smsHistory.getSentToGatewayDate());
				msg.setType(MessageHistory.TYPE_MESSAGE);
				msg.setResult(smsHistory.getSmsSentResult());
				listMessageHistory.add(msg);
			}
			
			List<AlertOutbox> listAlertOutbox = getAllAlertOutbox();
			for(AlertOutbox alertOutbox : listAlertOutbox){
				MessageHistory msg = new MessageHistory();
				msg.setProgramName(alertOutbox.getAlertName());
				msg.setPatient(alertOutbox.getPatient());
				msg.setDate(alertOutbox.getDateCreated());
				msg.setType(MessageHistory.TYPE_EMAIL);
				msg.setResult("Sent");
				listMessageHistory.add(msg);
			}
			
			List<AlertError> listAlertError = getAllAlertErrors();
			for(AlertError alertError : listAlertError){
				MessageHistory msg = new MessageHistory();
				msg.setProgramName(alertError.getAlertName());
				msg.setPatient(alertError.getPatient());
				msg.setDate(alertError.getDateCreated());
				msg.setType(MessageHistory.TYPE_EMAIL);
				msg.setResult("Not sent");
				listMessageHistory.add(msg);
			}
			
		}
		
		//sort before return result
		Collections.sort(listMessageHistory, new Comparator<MessageHistory>() {
	        @Override
	        public int compare(MessageHistory messageHistory1, MessageHistory messageHistory2) {
	        	//too much cases, so return 0 if any value = null
	        	if(messageHistory1 != null && messageHistory1.getDate() != null &&
	        			messageHistory2 != null && messageHistory2.getDate() != null) {
	        		return  -(messageHistory1.getDate().compareTo(messageHistory2.getDate()));
	        	} else {
	        		return 0;
	        	}
	        	
	        }
	    });
		
		ArrayList<MessageHistory> subListMsg = new ArrayList<MessageHistory>();
		for(int i = start; (i < length+start) &&(i < listMessageHistory.size()); i++){
			subListMsg.add(listMessageHistory.get(i));
		}
		return subListMsg;
	}
	
	public List<MessageHistory> getAllMessageHistories(){
		ArrayList<MessageHistory> listMessageHistory = new ArrayList<MessageHistory>();
		List<CallLog> listCallLog = getAllCallLogs();
		for(CallLog callLog : listCallLog){
			MessageHistory msg = new MessageHistory();
			msg.setProgramName(callLog.getCallTrigger().getName());
			msg.setPatient(callLog.getPatient());
			msg.setDate(callLog.getDateCreated());
			msg.setType(MessageHistory.TYPE_CALL);
			msg.setResult(callLog.getStatusMessage());
			listMessageHistory.add(msg);
		}
		
		List<SmsHistory> listSmsHistory = getAllSmsHistories();
		for(SmsHistory smsHistory : listSmsHistory){
			MessageHistory msg = new MessageHistory();
			msg.setProgramName(smsHistory.getProgramming().getName());
			msg.setPatient(smsHistory.getPatient());
			msg.setDate(smsHistory.getSentToGatewayDate());
			msg.setType(MessageHistory.TYPE_MESSAGE);
			msg.setResult(smsHistory.getSmsSentResult());
			listMessageHistory.add(msg);
		}
		
		List<AlertOutbox> listAlertOutbox = getAllAlertOutbox();
		for(AlertOutbox alertOutbox : listAlertOutbox){
			MessageHistory msg = new MessageHistory();
			msg.setProgramName(alertOutbox.getAlertName());
			msg.setPatient(alertOutbox.getPatient());
			msg.setDate(alertOutbox.getDateCreated());
			msg.setType(MessageHistory.TYPE_EMAIL);
			msg.setResult("Sent");
			listMessageHistory.add(msg);
		}
		
		List<AlertError> listAlertError = getAllAlertErrors();
		for(AlertError alertError : listAlertError){
			MessageHistory msg = new MessageHistory();
			msg.setProgramName(alertError.getAlertName());
			msg.setPatient(alertError.getPatient());
			msg.setDate(alertError.getDateCreated());
			msg.setType(MessageHistory.TYPE_EMAIL);
			msg.setResult("Not sent");
			listMessageHistory.add(msg);
		}
		
		return listMessageHistory;
	}
}