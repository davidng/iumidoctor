package org.openmrs.module.iumidoctor;

import java.util.Date;

import org.openmrs.Auditable;
import org.openmrs.BaseOpenmrsObject;
import org.openmrs.Concept;
import org.openmrs.Form;
import org.openmrs.User;

/**
 * POJO for the CallTrigger table, the trigger are the
 * cause agent for an post being made to odk-voice
 * @author Samuel Mbugua
 */

public class CallTrigger extends BaseOpenmrsObject implements Auditable{
	public static final int PREFERENCE_DAY = 0;
	public static final int PREFERENCE_HOUR = 1;
	public static final int PREFERENCE_BOTH = 2;
	
	private Integer id;
	private String name;
	private Form form;
	private Concept concept;
	private Integer triggerValue;
	private Integer triggerModifier;
	private Integer preference;
	private Integer savedSearch;
	private User creator;
	private Date dateCreated;
	private Concept visitDateConcept;
	private String verboiceChannel;
	private int verboiceProjectId;
	private int verboiceCallFlowId;
	private String verboiceSchedule;	
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the form
	 */
	public Form getForm() {
		return form;
	}
	/**
	 * @param form the form to set
	 */
	public void setForm(Form form) {
		this.form = form;
	}
	/**
	 * @return the concept
	 */
	public Concept getConcept() {
		return concept;
	}
	/**
	 * @param concept the concept to set
	 */
	public void setConcept(Concept concept) {
		this.concept = concept;
	}
	/**
	 * @return the triggerValue
	 */
	public Integer getTriggerValue() {
		return triggerValue;
	}
	/**
	 * @param triggerValue the triggerValue to set
	 */
	public void setTriggerValue(Integer triggerValue) {
		this.triggerValue = triggerValue;
	}
	/**
	 * @return the triggerModifier
	 */
	public Integer getTriggerModifier() {
		return triggerModifier;
	}
	/**
	 * @param triggerModifier the triggerModifier to set
	 */
	public void setTriggerModifier(Integer triggerModifier) {
		this.triggerModifier = triggerModifier;
	}
	/**
	 * @return the preference
	 */
	public Integer getPreference() {
		return preference;
	}
	/**
	 * @param preference the preference to set
	 */
	public void setPreference(Integer preference) {
		this.preference = preference;
	}
	/**
	 * @return the savedSearch
	 */
	public Integer getSavedSearch() {
		return savedSearch;
	}
	/**
	 * @param savedSearch the savedSearch to set
	 */
	public void setSavedSearch(Integer savedSearch) {
		this.savedSearch = savedSearch;
	}
	/**
	 * @return the creator
	 */
	public User getCreator() {
		return creator;
	}
	/**
	 * @param creator the creator to set
	 */
	public void setCreator(User creator) {
		this.creator = creator;
	}
	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}
	/**
	 * @return the visitDateConcept	
	 */
	public Concept getVisitDateConcept() {
		return visitDateConcept;
	}

	public void setVisitDateConcept(Concept visitDateConcept) {
		this.visitDateConcept = visitDateConcept;
	}

	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public String getVerboiceChannel() {
		return verboiceChannel;
	}
	
	public void setVerboiceChannel(String verboiceChannel) {
		this.verboiceChannel = verboiceChannel;
	}
	
	public int getVerboiceProjectId() {
		return verboiceProjectId;
	}
	
	public void setVerboiceProjectId(int verboiceProjectId) {
		this.verboiceProjectId = verboiceProjectId;
	}
	
	public int getVerboiceCallFlowId() {
		return verboiceCallFlowId;
	}
	
	public void setVerboiceCallFlowId(int verboiceCallFlowId) {
		this.verboiceCallFlowId = verboiceCallFlowId;
	}
	
	public String getVerboiceSchedule() {
		return verboiceSchedule;
	}
	
	public void setVerboiceSchedule(String verboiceSchedule) {
		this.verboiceSchedule = verboiceSchedule;
	}
	
	public User getChangedBy() {
		// TODO Auto-generated method stub
		return null;
	}
	public Date getDateChanged() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setChangedBy(User arg0) {
		// TODO Auto-generated method stub
		
	}
	public void setDateChanged(Date arg0) {
		// TODO Auto-generated method stub
		
	}
	public boolean isDayPrefference(){
		if (this.getPreference() != null 
				&& (this.getPreference() == PREFERENCE_BOTH || this.getPreference() == PREFERENCE_DAY))
			return true;
		
		return false;
	}
	public boolean isHourPrefference(){
		if (this.getPreference() != null 
				&& (this.getPreference() == PREFERENCE_BOTH || this.getPreference() == PREFERENCE_HOUR))
			return true;
		
		return false;
	}
	
}