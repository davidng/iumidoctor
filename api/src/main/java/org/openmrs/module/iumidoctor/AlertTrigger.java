package org.openmrs.module.iumidoctor;

import java.util.Date;

import org.openmrs.Auditable;
import org.openmrs.BaseOpenmrsObject;
import org.openmrs.User;

/**
 * POJO for the AlertTrigger table, the trigger are the
 * cause agent for an email being dispatched
 * @author Samuel Mbugua
 */
public class AlertTrigger extends BaseOpenmrsObject implements Auditable{
	private Integer id;
	private Integer savedSearch;
	private Integer form;
	private Integer concept;
	private String triggerValue;
	private User creator;
	private Date dateCreated;
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the savedSearch
	 */
	public Integer getSavedSearch() {
		return savedSearch;
	}
	/**
	 * @param savedSearch the savedSearch to set
	 */
	public void setSavedSearch(Integer savedSearch) {
		this.savedSearch = savedSearch;
	}
	/**
	 * @return the form
	 */
	public Integer getForm() {
		return form;
	}
	/**
	 * @param form the form to set
	 */
	public void setForm(Integer form) {
		this.form = form;
	}
	/**
	 * @return the concept
	 */
	public Integer getConcept() {
		return concept;
	}
	/**
	 * @param concept the concept to set
	 */
	public void setConcept(Integer concept) {
		this.concept = concept;
	}
	/**
	 * @return the triggerValue
	 */
	public String getTriggerValue() {
		return triggerValue;
	}
	/**
	 * @param triggerValue the triggerValue to set
	 */
	public void setTriggerValue(String triggerValue) {
		this.triggerValue = triggerValue;
	}
	/**
	 * @return the creator
	 */
	public User getCreator() {
		return creator;
	}
	/**
	 * @param creator the creator to set
	 */
	public void setCreator(User creator) {
		this.creator = creator;
	}
	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}
	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public User getChangedBy() {
		// TODO Auto-generated method stub
		return null;
	}
	public Date getDateChanged() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setChangedBy(User arg0) {
		// TODO Auto-generated method stub
		
	}
	public void setDateChanged(Date arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
}