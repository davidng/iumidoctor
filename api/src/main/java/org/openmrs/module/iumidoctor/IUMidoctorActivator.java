package org.openmrs.module.iumidoctor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.module.BaseModuleActivator;

/**
 * Activator startup/shutdown methods for the IUMidoctor module
 *
 * @author  Samuel Mbugua
 */
public class IUMidoctorActivator extends BaseModuleActivator {

	private Log log = LogFactory.getLog(this.getClass());
	
	@Override
	public void willStart() {
		super.willStart();
		log.info("Starting the Message History module");
	}
	
	@Override
	public void willStop() {
		super.willStop();
		log.info("Shutting down the Message History module");
	}	
}
