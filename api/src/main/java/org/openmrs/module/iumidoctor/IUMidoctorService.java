package org.openmrs.module.iumidoctor;

import java.util.List;

/**
 * Required methods for message history service
 * 
 * @author  Citigo
 */

public interface IUMidoctorService {

	/**
	 * Return all calls in database
	 * @return {@link CallLog}}
	 */
	public List<CallLog> getAllCallLogs();

	
	/**
	 * Return all sms histories in database
	 * @return {@link SmsHistory}}
	 */
	public List<SmsHistory> getAllSmsHistories();
	

	/**
	 * Return all AlertOutbox in database
	 * @return {@link AlertOutbox}}
	 */
	public List<AlertOutbox> getAllAlertOutbox();
	
	/**
	 * Return all AlertErrors in database
	 * @return {@link AlertErrors}}
	 */
	public List<AlertError> getAllAlertErrors();
	

	/**
	 * Return specific type of call logs
	 * @param start
	 * @param length
	 * @param status
	 * @return a list of {@link CallLog}
	 */
	public List<CallLog> getCallLogsByStatus(Integer start, Integer length, Integer status);
	
	
	/**
	 * Return all message history including call, message & email
	 */
	public List<MessageHistory> getAllMessageHistories();
	
	/**
	 * Return message history in a page
	 * @param start
	 * @param length
	 * @return a list of {@link MessageHistory}
	 */
	public List<MessageHistory> getMessageHistories(Integer messageType, Integer start, Integer length);
}