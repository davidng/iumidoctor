package org.openmrs.module.iumidoctor;

import java.util.Date;

import org.openmrs.Auditable;
import org.openmrs.BaseOpenmrsObject;
import org.openmrs.Patient;
import org.openmrs.User;

/**
 * POJO for the CallLog table, 
 * @author Samuel Mbugua
 */
public class CallLog extends BaseOpenmrsObject implements Auditable{
	private Integer id;
	private CallTrigger callTrigger;
	private Patient patient;
	private Integer status;
	private String statusMessage;
	private User creator;
	private Date dateCreated;
	private Integer obsId;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the callTrigger
	 */
	public CallTrigger getCallTrigger() {
		return callTrigger;
	}
	/**
	 * @param callTrigger the callTrigger to set
	 */
	public void setCallTrigger(CallTrigger callTrigger) {
		this.callTrigger = callTrigger;
	}
	/**
	 * @return the obsId
	 */
	public Integer getObsId() {
		return obsId;
	}
	/**
	 * @param obsId the obsId to set
	 */
	public void setObsId(Integer obsId) {
		this.obsId = obsId;
	}
	/**
	 * @return the patient
	 */
	public Patient getPatient() {
		return patient;
	}
	/**
	 * @param patient the patient to set
	 */
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * @return the statusMessage
	 */
	public String getStatusMessage() {
		return statusMessage;
	}
	/**
	 * @param statusMessage the statusMessage to set
	 */
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	/**
	 * @return the creator
	 */
	public User getCreator() {
		return creator;
	}
	/**
	 * @param creator the creator to set
	 */
	public void setCreator(User creator) {
		this.creator = creator;
	}
	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}
	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public User getChangedBy() {
		// TODO Auto-generated method stub
		return null;
	}
	public Date getDateChanged() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setChangedBy(User arg0) {
		// TODO Auto-generated method stub
	}
	public void setDateChanged(Date arg0) {
		// TODO Auto-generated method stub
	}
}