package org.openmrs.module.iumidoctor.db.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.openmrs.module.iumidoctor.CallLog;
import org.openmrs.module.iumidoctor.SmsHistory;
import org.openmrs.module.iumidoctor.AlertOutbox;
import org.openmrs.module.iumidoctor.AlertError;
import org.openmrs.module.iumidoctor.db.IUMidoctorDAO;

/**
 * Database interface for the module
 * 
 * @author Samuel Mbugua
 * 
 */
public class HibernateIUMidoctorDAO implements IUMidoctorDAO {

	/**
	 * Hibernate session factory
	 */
        
	private SessionFactory sessionFactory;

	/**
	 * Default public constructor
	 */
	public HibernateIUMidoctorDAO() {
	}

	/**
	 * Set session factory
	 * 
	 * @param sessionFactory
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@SuppressWarnings("unchecked")
	public List<CallLog> getAllCallLogs() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				CallLog.class);
		List<CallLog> results = new ArrayList<CallLog>();
		results = (List<CallLog>) criteria.list();
		return results;
	}

	@SuppressWarnings("unchecked")
	public List<CallLog> getCallLogsByStatus(Integer start, Integer length,
			Integer status) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				CallLog.class);
		criteria.add(Expression.like("status", status));
		if (start != null)
			criteria.setFirstResult(start);
		if (length != null && length > 0)
			criteria.setMaxResults(length);
		criteria.addOrder(Order.desc("dateCreated"));
		return (List<CallLog>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<SmsHistory> getAllSmsHistories(){
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				SmsHistory.class);
		List<SmsHistory> results = new ArrayList<SmsHistory>();
		results = (List<SmsHistory>) criteria.list();
		return results;
	}
	
	@SuppressWarnings("unchecked")
	public List<AlertOutbox> getAllAlertOutbox(){
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				AlertOutbox.class);
		List<AlertOutbox> results = new ArrayList<AlertOutbox>();
		results = (List<AlertOutbox>) criteria.list();
		return results;
	}
	
	@SuppressWarnings("unchecked")
	public List<AlertError> getAllAlertErrors(){
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				AlertError.class);
		List<AlertError> results = new ArrayList<AlertError>();
		results = (List<AlertError>) criteria.list();
		return results;
	}

}