package org.openmrs.module.iumidoctor;

import java.util.Date;

import org.openmrs.Auditable;
import org.openmrs.BaseOpenmrsObject;
import org.openmrs.Patient;
import org.openmrs.User;

/**
 * POJO for the AlertArchive table, it is an alert in its
 * processed form after successful dispatch
 * @author Samuel Mbugua
 */
public class AlertError extends BaseOpenmrsObject implements Auditable{
	private Integer id;
	private String alertName;
	private String recipients;
	private String roles;
	private String subject;
	private String message;
	private AlertTrigger alertTrigger;
	private Patient patient;
	private Date lastAttempt;
	private String lastAttemptReport;
	private User creator;
	private Date dateCreated;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the alertName
	 */
	public String getAlertName() {
		return alertName;
	}
	/**
	 * @param alertName the alertName to set
	 */
	public void setAlertName(String alertName) {
		this.alertName = alertName;
	}
	/**
	 * @return the recipients
	 */
	public String getRecipients() {
		return recipients == null ? "" : recipients;
	}
	/**
	 * @param recipients the recipients to set
	 */
	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(String roles) {
		this.roles = roles == null ? "" : roles;
	}

	/**
	 * @return the roles
	 */
	public String getRoles() {
		return roles;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the alertTrigger
	 */
	public AlertTrigger getAlertTrigger() {
		return alertTrigger;
	}
	/**
	 * @param alertTrigger the alertTrigger to set
	 */
	public void setAlertTrigger(AlertTrigger alertTrigger) {
		this.alertTrigger = alertTrigger;
	}
	/**
	 * @return the patient
	 */
	public Patient getPatient() {
		return patient;
	}
	/**
	 * @param patient the patient to set
	 */
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	/**
	 * @return the lastAttempt
	 */
	public Date getLastAttempt() {
		return lastAttempt;
	}
	/**
	 * @param lastAttempt the lastAttempt to set
	 */
	public void setLastAttempt(Date lastAttempt) {
		this.lastAttempt = lastAttempt;
	}
	/**
	 * @return the lastAttemptReport
	 */
	public String getLastAttemptReport() {
		return lastAttemptReport;
	}
	/**
	 * @param lastAttemptReport the lastAttemptReport to set
	 */
	public void setLastAttemptReport(String lastAttemptReport) {
		this.lastAttemptReport = lastAttemptReport;
	}
	/**
	 * @return the creator
	 */
	public User getCreator() {
		return creator;
	}
	/**
	 * @param creator the creator to set
	 */
	public void setCreator(User creator) {
		this.creator = creator;
	}
	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}
	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public User getChangedBy() {
		return null;
	}
	public Date getDateChanged() {
		return null;
	}
	public void setChangedBy(User arg0) {
		
	}
	public void setDateChanged(Date arg0) {
	}
}
