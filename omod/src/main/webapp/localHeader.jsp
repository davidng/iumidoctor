<ul id="menu">
	<li class="first">
		<a href="${pageContext.request.contextPath}/admin"><spring:message code="admin.title.short"/></a>
	</li>
	
	<openmrs:hasPrivilege privilege="View Message History">
		<li <c:if test='<%= request.getRequestURI().contains("iumidoctor/messageHistory") %>'>class="active"</c:if>>
			<a href="${pageContext.request.contextPath}/module/iumidoctor/messageHistory.list">
				<spring:message code="iumidoctor.view"/>
			</a>
		</li>
	</openmrs:hasPrivilege>

</ul>