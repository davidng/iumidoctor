<%@ include file="/WEB-INF/template/include.jsp" %>

<%@ page import = "org.openmrs.module.iumidoctor.MessageHistory, java.util.Map, java.util.HashMap" %>

<openmrs:require privilege="View Message History" otherwise="/login.htm" redirect="/module/iumidoctor/messageHistory.list"/>

<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp"%>
<openmrs:htmlInclude file="/scripts/dojo/dojo.js" />
<openmrs:htmlInclude file="/scripts/dojoConfig.js" />

<c:set var="messageHistorySize" value="${fn:length(messageHistory)}" />

<div>
	<b class="boxHeader">
		<spring:message code="iumidoctor.message.history.header" />
	</b>
	<c:choose>
		<c:when test="${messageHistorySize < 1}">
			<br/>&nbsp;&nbsp;<i>(<spring:message code="iumidoctor.null"/>)</i><br/>
		</c:when>
		<c:otherwise>
			<div class="box">
				<form method="post">
					<table>
						<tr>
							<td><spring:message
									code="iumidoctor.message.select.message.type" /></td>
							<td>
								<!-- ${MessageHistory.TYPE_EMAIL} -->
								<select name="messageType" onchange="window.parent.location = '${pageContext.request.contextPath}/module/iumidoctor/messageHistory.list?selectedMessageType=' + this.options[this.selectedIndex].value; return false;">
									<option value="">--Select All--</option>
									<option value="1" <c:if test="${selectedMessageType == 1}">selected</c:if>><spring:message code="iumidoctor.message.type.call"/></option>
									<option value="2" <c:if test="${selectedMessageType == 2}">selected</c:if>><spring:message code="iumidoctor.message.type.message"/></option>
									<option value="3" <c:if test="${selectedMessageType == 3}">selected</c:if>><spring:message code="iumidoctor.message.type.email"/></option>									
								</select>
							</td>
						</tr>
					</table>

					<table cellpadding="4">
						<tr>
							<th><u><spring:message code="iumidoctor.message.name"/></u></th>
							<th><u><spring:message code="iumidoctor.message.patient.name"/></u></th>
							<th><u><spring:message code="iumidoctor.message.date"/></u></th>
							<th><u><spring:message code="iumidoctor.message.type"/></u></th>
							<th><u><spring:message code="iumidoctor.message.result"/></u></th>
						</tr>
						<c:forEach items="${messageHistory}" var="msg" varStatus="varStatus">
							<tr class="<c:choose><c:when test="${varStatus.index % 2 == 0}">evenRow</c:when><c:otherwise>oddRow</c:otherwise></c:choose>">
								<td>${msg.programName}</td>
								<td><a href="${pageContext.request.contextPath}/patientDashboard.form?patientId=${msg.patient.id}">${msg.patient.personName.fullName}</a></td>
								<td><openmrs:formatDate date="${msg.date}" type="long" /></td>
								<td>${msg.typeName}</td>
								<td>${msg.result}</td>
							</tr>
						</c:forEach>
					</table>
					<input type="hidden" value=${page} name="page"/>
					<table width="100%" cellpadding="8">
						<tr>
							<td align="center">
								<c:choose>
									<c:when test="${page > 1}">
										<a href="javascript:void(0)" onClick="window.parent.location = '${pageContext.request.contextPath}/module/iumidoctor/messageHistory.list?page=${page - 2}&selectedMessageType=${selectedMessageType}'; return false;"><spring:message code="iumidoctor.page.previous"/></a>
									</c:when>
									<c:otherwise>
										<spring:message code="iumidoctor.page.previous"/>
									</c:otherwise>
								</c:choose> |
								<c:choose>
									<c:when test="${!endPage}">
										<a href="javascript:void(0)" onClick="window.parent.location = '${pageContext.request.contextPath}/module/iumidoctor/messageHistory.list?page=${page}&selectedMessageType=${selectedMessageType}'; return false;"><spring:message code="iumidoctor.page.next"/></a>
									</c:when>
									<c:otherwise>
										<spring:message code="iumidoctor.page.next"/>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</c:otherwise>
	</c:choose>
</div>
<br/>
<%@ include file="/WEB-INF/template/footer.jsp" %>