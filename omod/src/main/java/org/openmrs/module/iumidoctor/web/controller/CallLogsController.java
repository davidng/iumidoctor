package org.openmrs.module.iumidoctor.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.context.Context;
import org.openmrs.module.iumidoctor.CallLog;
import org.openmrs.module.iumidoctor.MessageHistory;
import org.openmrs.module.iumidoctor.IUMidoctorService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for all call logging pages
 * 
 * @author Samuel Mbugua
 */
@Controller
public class CallLogsController {
	
	/** Logger for this class and subclasses */
	protected final Log log = LogFactory.getLog(getClass());
	
	//a single instance of module service
	private IUMidoctorService css;
	
	@RequestMapping(value="/module/iumidoctor/messageHistory", method=RequestMethod.GET)
	public String getPostedCalls(ModelMap model, HttpServletRequest request) {
		getIUMidoctorService();
		//a status of 1 is a success
		
		Integer messageType = Integer.valueOf(MessageHistory.TYPE_ALL);
		try {
			messageType = Integer.valueOf(request.getParameter("selectedMessageType"));
		} catch (Exception ex) {
			//
			ex.printStackTrace();
		}
		
		Integer length = 30;
		Integer page = null;
		boolean endPage=false;
		try {
			page = Integer.parseInt(request.getParameter("page"));
		}catch (NumberFormatException e) {
			page=0;
		}
		
		Integer nextPage = page+1;
//		List<CallLog> callLogs = css.getCallLogsByStatus(nextPage*length, length, 1);		
//		if (callLogs.size() < 1 || callLogs == null)
//			endPage=true;
//		model.addAttribute("messageHistory", css.getCallLogsByStatus(page*length, length, 1));
		List<MessageHistory> listMessageHistory = css.getMessageHistories(messageType, nextPage*length, length);
		if(listMessageHistory.size() < 1 || listMessageHistory == null){
			endPage = true;
		}
		model.addAttribute("selectedMessageType", messageType);
		model.addAttribute("messageHistory", css.getMessageHistories(messageType, page*length, length));
		//model.addAttribute("messageHistory", css.getMessageHistories(page*length, length));
		model.addAttribute("endPage", endPage);
		model.addAttribute("page", nextPage);
		return "/module/iumidoctor/messageHistory";
	}
	
	
	private void getIUMidoctorService(){
		if (css == null)
			css=(IUMidoctorService)Context.getService(IUMidoctorService.class);
	}
}
