package org.openmrs.module.iumidoctor.extension.html;

import java.util.LinkedHashMap;
import java.util.Map;

import org.openmrs.module.Extension;
import org.openmrs.module.web.extension.AdministrationSectionExt;
import org.openmrs.util.OpenmrsClassLoader;

/**
 * Anchor for the module in the Main OpenMRS administration page
 * 
 * @author Samuel Mbugua
 *
 */
public class AdminList extends AdministrationSectionExt {
	
	private static String requiredPrivileges = "View Message History";
	

	public Extension.MEDIA_TYPE getMediaType() {
		return Extension.MEDIA_TYPE.html;
	}
	

	public String getTitle() {
		return "iumidoctor.title";
	}
	
	public String getRequiredPrivilege() {
		if (requiredPrivileges == null) {
			StringBuilder builder = new StringBuilder();
			requiredPrivileges = builder.toString();
		}
		
		return requiredPrivileges;
	}
	@Override
	public Map<String, String> getLinks() {
		
		//Map<String, String> map = new TreeMap<String, String>(new InsertedOrderComparator());
		Map<String, String> map = new LinkedHashMap<String, String>();
		Thread.currentThread().setContextClassLoader(OpenmrsClassLoader.getInstance());
		map.put("module/iumidoctor/messageHistory.list", "iumidoctor.view");
		return map;
	}
	
}
